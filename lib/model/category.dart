class Category {
  int id;
  String name;
  String photo;

  factory Category(Map jsonMap) => Category._internalFromJson(jsonMap);

  Category._internalFromJson(Map jsonMap)
      : id = jsonMap["id"],
        name = jsonMap["title"] ?? "",
        photo = jsonMap["photo"] ?? "";

  Map toJson() => {'id': id, 'title': name, 'photo': photo};

  factory Category.fromPrefsJson(Map jsonMap) =>
      Category._internalFromJson(jsonMap);
}
