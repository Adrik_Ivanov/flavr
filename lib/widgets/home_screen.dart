import 'package:flavr/utils/colors.dart';
import 'package:flavr/widgets/tabs/category_tab.dart';
import 'package:flavr/widgets/tabs/favorite_tab.dart';
import 'package:flavr/widgets/tabs/profile_tab.dart';
import 'package:flavr/widgets/tabs/recipes_tab.dart';
import 'package:flutter/material.dart';
import 'package:flavr/utils/in_memory_store.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  List<Widget> _children = [];
  List<Widget> _appBars = [];

  @override
  void initState() {
    _children.add(RecipesTab());
    _children.add(CategoryTab());
    _children.add(FavoriteTab());
    _children.add(ProfileTab());

    _appBars.add(_buildAppBar("Recipes"));
    _appBars.add(_buildAppBar("Category"));
    _appBars.add(_buildAppBar("Favorite"));
    _appBars.add(_buildAppBar("Profile"));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBars[_currentIndex],
      body: _children[_currentIndex],
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  Widget _buildAppBar(String title) {
    return AppBar(
      bottom: PreferredSize(
          child: Container(
            color: Colors.grey.shade200,
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(1.0)),
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      elevation: 2.0,
      title: Text(
        title,
        style: TextStyle(color: colorMain),
      ),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      onTap: _onTabTapped,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.restaurant_menu), title: Text("Recipes")),
        BottomNavigationBarItem(
            icon: Icon(Icons.featured_play_list), title: Text("Category")),
        BottomNavigationBarItem(
            icon: Icon(Icons.favorite), title: Text("Favorite")),
        BottomNavigationBarItem(
            icon: Icon(Icons.person), title: Text("Profile")),
      ],
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentIndex,
    );
  }

  _onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  void dispose() {
    InMemoryStore().clear();
    super.dispose();
  }
}
