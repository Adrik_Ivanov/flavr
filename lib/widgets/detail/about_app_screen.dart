import 'package:flutter/material.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flavr/utils/colors.dart';

class AboutAppScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('About Flavr'),
        ),
        body: Container(
            margin: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Container(
                    width: 110.0,
                    height: 110.0,
                    child: Image.asset('assets/images/profile.jpg'),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Meet Flavr\n\nFlavr is a simple recepies collection application where users can surf for recepies of theirfavorite dishes.'
                      'Users can also save their favorite recepies to view them collectivelyand ready anytime. '
                      'Feel the taste of your favorite dishes with Flavr.',
                  style: TextStyle(
                      fontFamily: fontRegular
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Brought to you by Devshub',
                  style: TextStyle(color: colorMain),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  'Version 1.0',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
        ),
    );
  }
}
