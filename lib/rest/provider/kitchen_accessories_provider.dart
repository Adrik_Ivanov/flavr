import 'dart:async';

import 'package:flavr/model/kitchen_accessories.dart';
import 'package:flavr/rest/api_client.dart';

abstract class KitchenAccessoriesProvider {
  Future<List<KitchenAccessories>> loadKitchenAccessories({int page: 1});
}

class KaProvider extends KitchenAccessoriesProvider {
  KaProvider();

  ApiClient _apiClient = ApiClient();

  @override
  Future<List<KitchenAccessories>> loadKitchenAccessories({int page: 1}) {
    return _apiClient.fetchKitchenAccessories(page: page);
  }
}